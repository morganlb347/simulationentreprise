
class Salaries():
    def __init__(self,nom,salaire,fonction):
        self.nomsalarié=nom
        self.sonsalaire=salaire
        self.lafonction=fonction
        self.Lesvoiturefonction=[]


    def get_lenomsalairié(self):
        """
        :return:le nom du salarié
        """
        return self.nomsalarié

    def set_lenomsalairié(self,lenomsalarié):
        """
        :param lenomsalarié:nouveau nom du salariés
        :return:change le nom du salariés
        """
        self.nomsalarié=lenomsalarié

    def get_salaire(self):
        """
        :return:le salaire du salariés
        """
        return self.sonsalaire

    def set_salaire(self,nvsalaire):
        """
        :param nvsalaire:son nouveau salaire
        :return:salaire modifié
        """
        self.sonsalaire=nvsalaire
        return print("le salaire est modifié")

    def salairemodifiesalairié(self,salarié,lesalaire):
        """
        :param salarié:un salariés
        :param lesalaire:le nouveau salaire du salariés en parametre
        :return:le salaire modifié d'un salariés
        """
        #conditions si la fonction du salairiés qui veut modifié un salaire n'est pas directeur alors cela créer un exception
        if self.lafonction==0:
            raise Exception("Vous devez etre directeur pour cette action")
        else:
            salarié.set_salaire(lesalaire)


    def get_fonction(self):
        """
        :return:la fonction du salarié est retourné
        """
        return self.lafonction

    def set_fonction(self, fonction):
        """
        :param fonction:nouvelle fonction
        :return:la fonction modifié
        """
        self.lafonction = fonction
        return print("sa fonction est changée")

    def get_voiturefonction(self):
        """
        :return:les voitures de fonction
        """
        return self.Lesvoiturefonction

    def ajoutvoiturefonction(self,lavoiture):
        """
        :param lavoiture:une nouvelle voiture de fonction
        :return:une voiture de fonction ajouté pour le salariés
        """
        self.Lesvoiturefonction.append(lavoiture)
        return print("voiture de fonction ajouté")

    def supprimervoiture(self,lavoiture):
        """
        :param lavoiture:la voiture a supprimer
        :return:voiture de fonction supprimer pour le salariés
        """
        self.Lesvoiturefonction.remove(lavoiture)
        return print("voiture de fonction supprimé")
