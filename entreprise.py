


class Entreprise:
    def __init__(self,nom,ca):
        listeSalaries=[]
        listebureaux=[]
        listevoiture=[]
        self.nomentreprise=nom
        self.liste_Bureaux=listebureaux
        self.liste_Salariés=listeSalaries
        self.liste_voiture=listevoiture
        self.ca=ca
    def get_ca(self):
        return self.ca


    def set_ca(self,ca):
        self.ca=ca
        return ca


    def get_listeSalaries(self):
        return self.liste_Salariés

    def set_listeSalaries(self,lesalariés):
        self.liste_Salariés=lesalariés


    def get_liste_Bureaux(self):
        return self.liste_Bureaux

    def set_liste_Bureaux(self, lesbureaux):
        self.liste_Bureaux=lesbureaux

    def get_liste_voiture(self):
        return self.liste_voiture
    def set_liste_Voiture(self, lesVoitures):
        self.liste_voiture = lesVoitures

    def get_nomentreprise(self):
       return self.nomentreprise

    def ajoutsalariés(self,unsalariés):
        self.liste_Salariés.append(unsalariés)

    def ajoutbureaux(self,unbureaux):
        self.liste_Bureaux.append(unbureaux)


    def ajoutvoiture(self,unevoiture):
        self.liste_voiture.append(unevoiture)

    def supprimervoiture(self,unevoiture):
        self.liste_voiture.remove(unevoiture)


    def supprimerbureaux(self,unbureaux):
        self.liste_Bureaux.remove(unbureaux)

    def supprimersalariés(self,salariés):
        self.liste_Salariés.remove(salariés)

    def lessalaires(self):
        total = 0
        for i in self.liste_Salariés:
            total = total + i.get_salaire()


        return total

    def get_unevoiture(self,i):

        return self.liste_voiture[i]

    def get_unbureaux(self, i):
        return  self.liste_Bureaux[i]

    def get_unsalariés(self, i):
        return self.liste_Salariés[i]


    def lesprixbureaux(self):
        total=0
        for i in self.liste_Bureaux:
          total=total+i.get_prix()


        return total


    def coutentretienvoiture(self):
        total = 0
        for i in self.liste_voiture:
            total = total + i.get_coutentretien()

        return total


    def prixventevoitures(self):
        total = 0
        for i in self.liste_voiture:
            total = total + i.get_prix()

        return total

