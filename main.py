# This is a sample Python script.
import random

from entreprise import Entreprise
from salaries import Salaries
from voiture import Voiture
from bureaux import Bureaux
from simulation import Simulation
from  creationdonnes import CreationDonnes
import names
from random_address import real_random_address

#lancement d'une simulation sur un entreprise pendant x mois
def unesimulation(nbmois,entreprise):
   """
   :param nbmois:le nombre de mois a simulés
   :param entreprise:l'entreprise
   :return:la simulation sur x mois d'une entreprise
   """

   if nbmois>0 or nbmois<120:
      Lasimulation=Simulation(entreprise,nbmois)
      Lasimulation.unesimulation()
   else:
      raise Exception("le nombre de mois doit pas etre negatif ou nombre de mois supérieur a 120 ")


def listesalariés(nbsalariés):
   """
      :param nbbureaux:nombre de salariés a générer
      :return:la liste des  salariés généré
   """
   createsalairiés=CreationDonnes()
   malistesalariés=[]
   for i in range (nbsalariés):

      if i==0:
         malistesalariés.append(createsalairiés.create_directeur())
      else:
            malistesalariés.append(createsalairiés.create_salariés())


   return malistesalariés





def listebureaux(nbbureaux,entreprise):
   """
   :param nbbureaux:nombre de bureaux a générer
   :return:la liste des bureaux généré
   """
   # initalisation de classe qui permet de créer un bureaux
   createbureaux=CreationDonnes()
   # initalisation de la liste de bureaux
   malistebureaux=[]
   # boucle qui a  chaque itérations créer une voiture avec un propriétaire et ajoute a la liste de voiture
   # et change son occupation pour faire un bureaux non libres
   for i in range (nbbureaux):

      unbureaux1=createbureaux.create_bureaux()
      unbureaux1.set_occupation()
      unbureaux1.set_entreprise(entreprise)
      malistebureaux.append(unbureaux1)


   return malistebureaux


def listeVoiture(nbvoiture,uneentreprise):
   """
   :param nbvoiture:le nombre de voiture a générées
   :param uneentreprise:une entreprise propriétaire de ces voitures
   :return:liste objet voiture
   """
   #initalisation de classe qui permet de créer une voiture
   createvoiture=CreationDonnes()
   #initalisation de la liste de voiture
   malistevoiture=[]

   #boucle qui a  chaque itérations créer une voiture avec un propriétaire et ajoute a la liste de voiture
   for i in range (nbvoiture):
      malistevoiture.append(createvoiture.create_voiture(uneentreprise))

   return malistevoiture




# Press the green button in the gutter to run the script.
if __name__ == '__main__':
   #création des entreprise
   entreprise1=Entreprise("google",150000)
   entreprise2=Entreprise("microsoft",1500000)

   #ajout des listes de salariés,de bureauxn,de voiture
   entreprise1.set_listeSalaries(listesalariés(5))

   entreprise1.set_liste_Bureaux(listebureaux(5,entreprise1))


   entreprise1.set_liste_Voiture(listeVoiture(6,entreprise1))

   "/////////////////////////////////////////////////////////"
   entreprise2.set_listeSalaries(listesalariés(7))

   entreprise2.set_liste_Bureaux(listebureaux(5,entreprise2))

   entreprise2.set_liste_Voiture(listeVoiture(8, entreprise1))

   nombredemois=int(input("le nombres de mois?\n"))
   print("1:"+entreprise1.get_nomentreprise())
   print("2:" + entreprise2.get_nomentreprise())
   choixentreprise = int(input("choix entreprise?\n"))
   if choixentreprise==1:
      unesimulation(nombredemois,entreprise1)
   elif choixentreprise==2:
      unesimulation(nombredemois, entreprise2)










