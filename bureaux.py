
class Bureaux():

    def __init__(self,adresse,prix):
        self.adresse=adresse
        self.prix=prix
        self.occupation=0
        self.lentreprise=None

    def get_prix(self):
        """
        :return:le prix du bureaux
        """
        return self.prix

    def set_prix(self,prix):
        """
        :param prix:nv prix
        :return: changée prix
        """
        self.prix=prix
        return print("changement de prix fait")

    def get_adresse(self):
        """
        :return:adresse du bureaux
        """
        return self.adresse

    def set_adresse(self,adresse):
        """
        :param adresse:nv adresse
        :return:change adresse
        """
        self.adresse=adresse
        return print("adresse fait")

    def get_occupation(self):
        """
        :return: si le bureaux est occupés
        """
        return self.occupation

    def set_occupation(self):
        """
        :return:si le bureaux est occupé alors le bureaux de libre et inversevement
        """
        if self.occupation==0:
            self.occupation==1
        else:
            self.occupation == 0

    def get_entreprise(self):
        """
        :return: l'entreprise
        """
        return self.lentreprise

    def set_entreprise(self, entreprise):
        """
        :param entreprise:nouvelle entreprise
        :return: nouvelle entreprise
        """
        return self.lentreprise == entreprise
