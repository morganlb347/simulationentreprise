
import matplotlib.pyplot as plt
import numpy as np
import random
from creationdonnes import CreationDonnes
import names
from entreprise import Entreprise
from random_address import real_random_address

from voiture import Voiture


class Simulation():

    def __init__(self,uneentreprise,nbmois):
        self.uneentreprise=uneentreprise
        self.nbmois=nbmois



    def unesimulation(self):
      """
      :return: les graphiques par mois
      """
      #initialisation un objet donnes pour créer voiture....
      unedonnes=CreationDonnes()
      #initialisation de l index
      i=0
      #boucle tant que i est inférieur au nombre de mois
      while i<self.nbmois:
        #genere un nombre random compris entre 0 et 6
        randomnombre=random.randint(0,6)
        if randomnombre==0:
            #ajout d'un salariés
            self.uneentreprise.ajoutsalariés(unedonnes.create_salariés())
        elif randomnombre==1:
            # ajout d'une voiture
            self.uneentreprise.ajoutvoiture(unedonnes.create_voiture(self.uneentreprise))
        elif randomnombre==2:
            # ajout d'un bureaux
            self.uneentreprise.ajoutbureaux(unedonnes.create_bureaux())
        elif randomnombre == 3:
            #si la liste de  voiture n'est pas vide alors:
            if len(self.uneentreprise.get_listeSalaries()) != 0:
                #choisi dans la liste une voiture random et le supprime
                unevoiture=random.choice(self.uneentreprise.get_liste_voiture())
                self.uneentreprise.supprimervoiture(unevoiture)
        elif randomnombre==4:
            # si la liste de salariés n'est pas vide alors:
            if len(self.uneentreprise.get_listeSalaries())!=0:
                # choisi dans la liste un salarié random et le supprime
                lesalariés=random.choice(self.uneentreprise.get_listeSalaries())
                self.uneentreprise.supprimersalariés(lesalariés)
        elif randomnombre==5:
            # si la liste de bureaux n'est pas vide alors:
            if len(self.uneentreprise.get_liste_Bureaux())!=0:
                # choisi dans la liste un salarié random et le supprime
                lebureaux=random.choice(self.uneentreprise.get_liste_Bureaux())
                self.uneentreprise.supprimerbureaux(lebureaux)

        elif randomnombre==6:
            #créer un graphique du mois et incremente l index
            self.graphique(i+1)
            i = i + 1





    def graphique(self,i):
        """
        :param i:le mois
        :return:le graphique avec les cout mensuel et prix de vente
        """
        # pour créer le graphique il faut créer une première liste qui  liste les valeurs
        height = [self.uneentreprise.prixventevoitures(),self.uneentreprise.lesprixbureaux(), self.uneentreprise.lessalaires(),
                  self.uneentreprise.coutentretienvoiture()]

        # puis deuxieme list avec les nom des catégories
        bars = ("prix vente voiture","achatbureaux", "lessalaire","cout d'entretien")

        x_pos = np.arange(len(bars))

        # Create bars and choose color
        plt.bar(x_pos, height, color=(0.5, 0.1, 0.5, 0.6))

        # Add title and axis names
        plt.title('les charges et valeur entreprise:   '+self.uneentreprise.get_nomentreprise()+'  sur le mois'+str(i))
        plt.xlabel('categories')
        plt.ylabel('values')

        # Create names on the x axis
        plt.xticks(x_pos, bars)

        # Show graph
        plt.show()


