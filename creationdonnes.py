from random_address import real_random_address
import random
from bureaux import Bureaux
from salaries import Salaries
from voiture import Voiture
import names


class CreationDonnes():
    def __init__(self):
        self.i=0

    def create_salariés(self):
        """
        :return:un nouveau salariés
        """
        #utilise la fonction get_full_name() pour créer un nom random et randint pour créer un salaire
        salariés = Salaries(names.get_full_name(), random.randint(1200, 1700), 0)
        return salariés

    def create_voiture(self,uneentreprise):
        """
        :param uneentreprise:le propriétaire
        :return:une nouvelle voiture
        """
        #utilise randint pour générer aléatoire un prix et un cout entretien
        voiture= Voiture(uneentreprise, random.randint(2000, 3000), random.randint(1200, 1700))

        return voiture

    def create_bureaux(self):
        """
        :return:un nouveaux bureaux
        """
        #utilise real_random_adresse pour créer un nouvelle adresse
        unbureaux1 = Bureaux(real_random_address, random.randint(1500, 2000))

        return unbureaux1

    def create_directeur(self):
        """
        :return:un nouveau directeur
        """
        directeur = Salaries(names.get_full_name(), random.randint(2000, 2700), 1)

        return directeur


